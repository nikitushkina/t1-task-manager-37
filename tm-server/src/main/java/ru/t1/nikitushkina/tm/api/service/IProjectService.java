package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    void changeProjectStatusById(@Nullable String userId,
                                 @Nullable String id,
                                 @Nullable Status status
    ) throws Exception;

    void changeProjectStatusByIndex(@Nullable String userId,
                                    @Nullable Integer index,
                                    @Nullable Status status
    ) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Project create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description
    ) throws Exception;

    void updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description
    ) throws Exception;

    void updateByIndex(@Nullable String userId,
                       @Nullable Integer index,
                       @Nullable String name,
                       @Nullable String description
    ) throws Exception;

}
