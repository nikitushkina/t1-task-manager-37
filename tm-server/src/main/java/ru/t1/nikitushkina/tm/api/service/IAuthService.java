package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Session;
import ru.t1.nikitushkina.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable Session session) throws Exception;

    @NotNull
    User registry(@Nullable String login,
                  @Nullable String password,
                  @Nullable String email,
                  @Nullable String fstName,
                  @Nullable String lstName,
                  @Nullable String mdlName
    ) throws Exception;

    @NotNull
    Session validateToken(@Nullable String token) throws Exception;

}
