package ru.t1.nikitushkina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.nikitushkina.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nikitushkina.tm.api.endpoint.IProjectEndpoint;
import ru.t1.nikitushkina.tm.api.endpoint.IUserEndpoint;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.dto.request.*;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.marker.IntegrationCategory;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.service.PropertyService;

import java.util.List;

import static ru.t1.nikitushkina.tm.constant.ProjectTestData.*;
import static ru.t1.nikitushkina.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.nikitushkina.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectId1;

    private int projectIndex1;

    @Nullable
    private String projectId2;

    private int projectIndex2;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        userEndpointClient.registryUser(request);
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(USER_TEST_LOGIN);
        userLoginRequest.setPassword(USER_TEST_PASSWORD);
        userToken = authEndpointClient.login(userLoginRequest).getToken();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(request);
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(projectEndpointClient.clearProject(request));
    }

    @Before
    public void before() {
        projectId1 = createTestProject(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);
        projectIndex1 = 0;
        projectId2 = createTestProject(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);
        projectIndex2 = 1;
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIdRequest projectCreateRequestNullToken = new ProjectChangeStatusByIdRequest(null);
        projectCreateRequestNullToken.setProjectId(projectId1);
        projectCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> projectEndpointClient.changeProjectStatusById(projectCreateRequestNullToken));
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setProjectId(projectId1);
        request.setStatus(status);
        Assert.assertNotNull(projectEndpointClient.changeProjectStatusById(request));
        @Nullable final Project project = findProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIndexRequest projectCreateRequestNullToken = new ProjectChangeStatusByIndexRequest(null);
        projectCreateRequestNullToken.setIndex(projectIndex1);
        projectCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> projectEndpointClient.changeProjectStatusByIndex(projectCreateRequestNullToken));
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        request.setStatus(status);
        Assert.assertNotNull(projectEndpointClient.changeProjectStatusByIndex(request));
        @Nullable final Project project = findProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearProject() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(projectEndpointClient.clearProject(request));
        @Nullable Project project = findProjectById(projectId1);
        Assert.assertNull(project);
        project = findProjectById(projectId2);
        Assert.assertNull(project);
    }

    @Test
    public void completeById() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken);
        request.setProjectId(projectId1);
        Assert.assertNotNull(projectEndpointClient.completeById(request));
        @Nullable Project project = findProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void completeByIndex() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        Assert.assertNotNull(projectEndpointClient.completeByIndex(request));
        @Nullable Project project = findProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void createProject() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(USER_PROJECT3_NAME);
        projectCreateRequest.setDescription(USER_PROJECT3_DESCRIPTION);
        @Nullable Project project = projectEndpointClient.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

    @NotNull
    private String createTestProject(final String name, final String description) {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        return projectEndpointClient.createProject(projectCreateRequest).getProject().getId();
    }

    @Nullable
    private Project findProjectById(final String id) {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setProjectId(id);
        return projectEndpointClient.showById(request).getProject();
    }

    @Test
    public void listProject() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        @Nullable final List<Project> projects = projectEndpointClient.listProject(request).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        for (Project project : projects) {
            Assert.assertNotNull(findProjectById(project.getId()));
        }
    }

    @Test
    public void removeById() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setProjectId(projectId2);
        Assert.assertNotNull(projectEndpointClient.removeById(request));
        Assert.assertNull(findProjectById(projectId2));
    }

    @Test
    public void removeByIndex() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(userToken);
        request.setIndex(projectIndex2);
        Assert.assertNotNull(projectEndpointClient.removeByIndex(request));
        Assert.assertNull(findProjectById(projectId2));
    }

    @Test
    public void showById() {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setProjectId(projectId1);
        @Nullable final Project project = projectEndpointClient.showById(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId1, project.getId());
    }

    @Test
    public void showByIndex() {
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        @Nullable final Project project = projectEndpointClient.showByIndex(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId1, project.getId());
    }

    @Test
    public void startById() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken);
        request.setProjectId(projectId1);
        Assert.assertNotNull(projectEndpointClient.startById(request));
        @Nullable Project project = findProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void startByIndex() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        Assert.assertNotNull(projectEndpointClient.startByIndex(request));
        @Nullable Project project = findProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void updateById() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setProjectId(projectId1);
        request.setName(USER_PROJECT3_NAME);
        request.setDescription(USER_PROJECT3_DESCRIPTION);
        Assert.assertNotNull(projectEndpointClient.updateById(request));
        @Nullable Project project = findProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        request.setName(USER_PROJECT3_NAME);
        request.setDescription(USER_PROJECT3_DESCRIPTION);
        Assert.assertNotNull(projectEndpointClient.updateByIndex(request));
        @Nullable Project project = findProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

}
