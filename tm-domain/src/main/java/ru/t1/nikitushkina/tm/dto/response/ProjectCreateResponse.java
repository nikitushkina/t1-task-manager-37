package ru.t1.nikitushkina.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Project;

@NoArgsConstructor
public class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable Project project) {
        super(project);
    }

}
