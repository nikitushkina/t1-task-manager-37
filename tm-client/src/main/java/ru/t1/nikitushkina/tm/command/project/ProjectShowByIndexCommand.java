package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.ProjectShowByIndexRequest;
import ru.t1.nikitushkina.tm.dto.response.ProjectShowByIndexResponse;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show project by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @Nullable final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final ProjectShowByIndexResponse response = getProjectEndpoint().showByIndex(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
