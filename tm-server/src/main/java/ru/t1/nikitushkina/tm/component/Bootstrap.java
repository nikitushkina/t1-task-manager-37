package ru.t1.nikitushkina.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.endpoint.*;
import ru.t1.nikitushkina.tm.api.repository.ISessionRepository;
import ru.t1.nikitushkina.tm.api.service.*;
import ru.t1.nikitushkina.tm.endpoint.*;
import ru.t1.nikitushkina.tm.repository.SessionRepository;
import ru.t1.nikitushkina.tm.service.*;
import ru.t1.nikitushkina.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.nikitushkina.tm.command";
    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final Connection connection = connectionService.getConnection();
    @Getter
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository(connection);
    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);
    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);
    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);
    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService, projectService, taskService);
    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);
    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void initLogger() {
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        initPID();
        initLogger();
    }

}
