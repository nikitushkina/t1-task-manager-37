package ru.t1.nikitushkina.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    private List<Project> projects;

    public ProjectListResponse(@Nullable List<Project> projects) {
        this.projects = projects;
    }

}
