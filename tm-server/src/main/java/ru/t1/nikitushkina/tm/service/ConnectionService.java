package ru.t1.nikitushkina.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.service.IConnectionService;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public @NotNull Connection getConnection() {
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final String url = propertyService.getDBUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
