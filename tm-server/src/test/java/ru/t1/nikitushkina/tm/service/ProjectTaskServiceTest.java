package ru.t1.nikitushkina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.nikitushkina.tm.api.repository.IUserRepository;
import ru.t1.nikitushkina.tm.api.service.*;
import ru.t1.nikitushkina.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nikitushkina.tm.exception.entity.TaskNotFoundException;
import ru.t1.nikitushkina.tm.exception.field.ProjectIdEmptyException;
import ru.t1.nikitushkina.tm.exception.field.TaskIdEmptyException;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.marker.UnitCategory;
import ru.t1.nikitushkina.tm.model.Task;
import ru.t1.nikitushkina.tm.model.User;
import ru.t1.nikitushkina.tm.repository.UserRepository;

import java.sql.Connection;

import static ru.t1.nikitushkina.tm.constant.ProjectTestData.*;
import static ru.t1.nikitushkina.tm.constant.TaskTestData.*;
import static ru.t1.nikitushkina.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);

    @NotNull
    private static String userId = "";

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectService, taskService);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = userRepository.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = userRepository.findByLogin(USER_TEST_LOGIN);
        if (user != null) userRepository.remove(user);
    }

    @Before
    public void before() throws Exception {
        projectService.add(userId, USER_PROJECT1);
        projectService.add(userId, USER_PROJECT2);
        taskService.add(userId, USER_TASK1);
        taskService.add(userId, USER_TASK2);
    }

    @After
    public void after() throws Exception {
        taskService.clear(userId);
        projectService.clear(userId);
    }

    @Test
    public void bindTaskToProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(userId, null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(userId, "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(userId, USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(userId, USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.bindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.bindTaskToProject(userId, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.bindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK1.getId());
        @Nullable final Task task = taskService.findOneById(userId, USER_TASK1.getId());
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById("", USER_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(userId, null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(userId, "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.removeProjectById(userId, NON_EXISTING_PROJECT_ID);
        });
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK2.getId());
        service.removeProjectById(userId, USER_PROJECT1.getId());
        Assert.assertNull(projectService.findOneById(USER_PROJECT1.getId()));
        Assert.assertNull(taskService.findOneById(USER_TASK1.getId()));
        Assert.assertNull(taskService.findOneById(USER_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(userId, null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(userId, "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.unbindTaskFromProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final Task task = taskService.findOneById(userId, USER_TASK1.getId());
        Assert.assertNull(task.getProjectId());
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

}
