package ru.t1.nikitushkina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@Nullable Comparator comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull Integer index) throws Exception;

    int getSize() throws Exception;

    @Nullable
    M remove(@NotNull M model) throws Exception;

    void removeAll(@NotNull Collection<M> collection) throws Exception;

}
